//Khai báo thư viện express
const express = require("express");

//Khai báo middleware 
const {
    getAllOderMiddleware,
    getAOderMiddleware,
    postOderMiddleware,
    putOderMiddleware,
    deleteOderMiddleware
    } = require(`../middleware/oderMiddleware`);

//Tạo router
const OrderRouter = express.Router();

//Sử dụng Router
OrderRouter.get("/Order",getAllOderMiddleware,(req,res)=>{
    res.json({
        message:"Get all Order"
    })
});
OrderRouter.get("/Order/:OrderId",getAOderMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});
OrderRouter.post("/Order",postOderMiddleware,(req,res)=>{
    res.json({
        message:"Post a Order"
    })
});
OrderRouter.put("/Order/:OrderId",putOderMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});
OrderRouter.delete("/Order/:OrderId",deleteOderMiddleware,(req,res)=>{
    let OrderId = req.params.OrderId;
    res.json({
        message:`OrderId = ${OrderId}`
    })
});

module.exports = {OrderRouter}
