// Khai báo thư viện express
const express = require("express");
const { DrinkRouter } = require("./app/router/drinhkRouter");
const { OrderRouter } = require("./app/router/orderRouter");
const { UserRouter } = require("./app/router/userRouter");
const { VoucherRouter } = require("./app/router/voucherRouter");

const app = express();

const prot = 8000;

//sử dụng được body json
app.use(express.json());





// Sử dụng router
app.use("/",DrinkRouter);
app.use("/",VoucherRouter);
app.use("/",UserRouter);
app.use("/",OrderRouter);


// cổng chạy trên cổng 8000
app.listen(prot, ()=>{
    console.log("app listen on prot", prot)
});